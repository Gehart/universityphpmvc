<?php

// добавляет класс error, если ошибка присутствует
function addErrorClassIfExist($data, $fieldName) {
    if(isset($data['validation']['errors'][$fieldName])) {
        return 'error';
    }
}

// вставляет сообщение об ошибке
function getErrorMessage($data, $fieldName) {
    if(isset($data['validation']['errors'][$fieldName])) {
        return $data['validation']['errors'][$fieldName];
    }
}

// вставляем ранее введенные значения
function getFieldValueIfExist($data, $fieldName) {
    if(isset($data['value'][$fieldName])) {
        return $data['value'][$fieldName];
    }
}
