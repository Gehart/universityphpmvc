<div class="content-wrapper">
    <div class="container">
        <div class="content">
            <h2>Содержание</h2>
            <?php
                echo '<ul>'; 
                foreach($data as $hobby) {
                    echo "<li>
                            <a href=\"#{$hobby['categoryId']}\"> {$hobby['title']} </a>
                          </li>";
                } 
                echo '</ul>';
            ?>
        </div>
        <div class="text-content">
            <?php 
                foreach($data as $hobby) {
                    echo '<div class="article ' .$hobby['categoryId']. '" id="' .$hobby['categoryId']. '">
                            <h3>' .$hobby['title']. '</h3>
                            <p>' .$hobby['description']. '</p>
                          </div>';
                } 
            ?>
        </div>
    </div>
</div>