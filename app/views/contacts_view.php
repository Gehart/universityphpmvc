<?php
// включаем функции для обработки формы
include 'app/views/views_functions.php';
?>

<div class="form-wrapper">
    <form action="" method="POST" class="form" id="formContact" onsubmit="">
        <h2>Форма для связи</h2>
        <div class="fio-input input-field <?= addErrorClassIfExist($data, 'fio') ?>">
            <input id="fio" name="fio" placeholder="ФИО" type="text" value="<?= getFieldValueIfExist($data, 'fio') ?>">
            <span class="error-message"><?= getErrorMessage($data, 'fio') ?></span>
        </div>
        <div class="gender-input input-field <?= addErrorClassIfExist($data, 'gender') ?>">
            <p> Выберите пол</p>
            <div class="radio">
                <input id="male" name="gender" type="radio" value="male">
                <label for="male">Мужской</label>
                <input id="female" name="gender" type="radio" value="female">
                <label for="female">Женский</label>
                <span class="error-message"><?= getErrorMessage($data, 'gender') ?></span>
            </div>
        </div>
        <div class="age-input input-field <?= addErrorClassIfExist($data, 'birthday') ?>">
            <input name="birthday" placeholder="Дата рождения" readonly="readonly" type="text" value="<?= getFieldValueIfExist($data, 'birthday') ?>">
            <div class="calendar hidden" id="calendar">
                <div class="pick-year-and-month">
                    <select form="#" id="pick-month" name="pick-month"></select>
                    <select form="#" id="pick-year" name="pick-year"></select>
                </div>
                <div class="name-of-day">
                    <span>Пн</span>
                    <span>Вт</span>
                    <span>Ср</span>
                    <span>Чт</span>
                    <span>Пт</span>
                    <span>Сб</span>
                    <span>Вс</span>
                </div>
                <div class="pick-date">
                    <div class="empty-square"></div>
                </div>
            </div>
            <span class="error-message"><?= getErrorMessage($data, 'birthday') ?></span>
        </div>
        <div class="tel-input input-field <?= addErrorClassIfExist($data, 'tel') ?>">
            <input name="tel" placeholder="Введите телефон" type="text" value="<?= getFieldValueIfExist($data, 'tel') ?>">
            <span class="error-message"><?= getErrorMessage($data, 'tel') ?></span>
        </div>
        <div class="email-input input-field <?= addErrorClassIfExist($data, 'email') ?>">
            <input name="email" placeholder="Введите email" type="email" value="<?= getFieldValueIfExist($data, 'email') ?>">
            <span class="error-message"><?= getErrorMessage($data, 'email') ?></span>
        </div>
        <div class="textarea-input input-field <?= addErrorClassIfExist($data, 'textarea') ?>">
            <label for="textarea">
                Напишите сообщение
                <span class="error-message"><?= getErrorMessage($data, 'textarea') ?></span>
            </label>
            <textarea class="textarea" cols="25" id="textarea" name="textarea" placeholder="Напишите что-нибудь" rows="4"><?= getFieldValueIfExist($data, 'textarea') ?></textarea>
        </div>
        <div class="buttons-wrapper">
            <button type="reset">Очистить данные</button>
            <button type="submit">Отправить</button>
        </div>
    </form>
    <div class="modal-window" id="modal-window"></div>
</div>
<script src="public/assets/libs/jquery-3.5.1.min.js"></script>
<script src="public/assets/js/forms.js"></script>