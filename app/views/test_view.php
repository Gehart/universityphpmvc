<?php
// включаем функции для обработки формы
include 'app/views/views_functions.php';
?>

<div class="form-wrapper">
    <form method="POST" action="" class="form" id="formTest">
        <h2>
            Тест по дисциплине<br>"Основы экологии"
        </h2>
        <div class="fio-input input-field <?= addErrorClassIfExist($data, 'fio') ?>">
            <input name="fio" placeholder="ФИО" type="text" value="<?= getFieldValueIfExist($data, 'fio') ?>">
            <span class="error-message"><?= getErrorMessage($data, 'fio') ?></span>
        </div>
        <div class="group-input input-field <?= addErrorClassIfExist($data, 'group') ?>">
            <input name="group" placeholder="Группа" type="text" value="<?= getFieldValueIfExist($data, 'group') ?>">
            <span class="error-message"><?= getErrorMessage($data, 'group') ?></span>
        </div>
        <div class="eco-select-input input-field <?= addErrorClassIfExist($data, 'eco-select') ?>">
            <label for="eco-select">
                Должны ли мы заботиться об экологии?
                <span class="error-message"><?= getErrorMessage($data, 'eco-select') ?></span>
            </label>
            <select id="eco-select" name="eco-select" placeholder="Должны ли мы заботиться об экологии?" size="1">
                <option disabled="disabled" selected="selected" value="">Выберите ответ</option>
                <option value="1">Да, должны</option>
                <option value="2">Нет, в этом нет необходимости</option>
                <option value="3">Затрудняюсь ответить</option>
            </select>
        </div>
        <div class="eco-textarea-input input-field <?= addErrorClassIfExist($data, 'eco-textarea') ?>">
            <label for="eco-textarea">
                Опишите, что мы можем сделать для экологии?
                <span class="error-message"><?= getErrorMessage($data, 'eco-textarea') ?></span>
            </label>
            <textarea class="textarea" cols="25" id="eco-textarea" name="eco-textarea" placeholder="Напишите что-нибудь" rows="4"><?= getFieldValueIfExist($data, 'eco-textarea') ?></textarea>
        </div>
        <div class="eco-radio-input input-field <?= addErrorClassIfExist($data, 'garbage') ?>">
            <label for="eco-radio">Поддерживаете ли вы раздельный сбор мусора?</label>
            <div class="radio">
                <input id="yes" name="garbage" type="radio" value="yes">
                <label for="yes">Да, поддерживаю</label>
                <input id="no" name="garbage" type="radio" value="no">
                <label for="no">Нет</label>
                <span class="error-message"><?= getErrorMessage($data, 'garbage') ?></span>
            </div>
        </div>
        <div class="email-input input-field <?= addErrorClassIfExist($data, 'email') ?>">
            <input name="email" placeholder="Введите email" type="email" value="<?= getFieldValueIfExist($data, 'email') ?>">
            <span class="error-message"><?= getErrorMessage($data, 'email') ?></span>
        </div>
        <div class="buttons-wrapper">
            <button type="reset">Очистить данные</button>
            <button type="submit">Отправить</button>
        </div>
    </form>
</div>
<script src="public/assets/libs/jquery-3.5.1.min.js"></script>
<script src="public/assets/js/forms.js">