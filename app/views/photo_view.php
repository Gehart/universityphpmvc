<div class="photo-wrapper">
<?php 

    $counter = 0;
    foreach($data as $imgData) {
        $counter++;
        createImgElement($imgData['name'], $imgData['description'], 'img'.$counter);
    }


    function createImgElement($filename, $title, $imgId) {
        echo  '<figure class="img-block">
                    <div class="img-wrapper">
                        <img src="'.$filename.'" id="'.$imgId.'" alt="'.$title.'" title="'.$title.'">
                    </div>
                    <figcaption>'.$title.'</figcaption>
                </figure>';
    }

?>
</div>
<div class="big-img hidden" id="big-img">
    <img alt="" src="public/assets/img/cat1.jpg">
    <div class="big-img-control">
        <div class="arrow arrow-left">
            <i aria-hidden="true" class="fa fa-arrow-left"></i>
        </div>
        <div class="img-counter">
            <span>Фото</span>
            <span class="current-img-number">9</span>
            <span>из</span>
            <span class="biggest-img-number">15</span>
        </div>
        <div class="arrow arrow-right">
            <i aria-hidden="true" class="fa fa-arrow-right"></i>
        </div>
    </div>
</div>

<script src="public/assets/libs/jquery-3.5.1.min.js"></script>
<script src="public/assets/js/photo.js"></script>