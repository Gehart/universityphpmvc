<header class='header'>
    <div class='container'>
        <ul class='menu-list'>
            <li>
                <a href='main'>Главная</a>
            </li>
            <li>
                <a href='about_me'>Обо мне</a>
            </li>
            <li>
                <a href='hobby'>Мои интересы</a>
                <ul class='sub-menu-list'>
                    <li>
                        <a href='hobby#hobby'>Мои хобби</a>
                    </li>
                    <li>
                        <a href='hobby#books'>Мои любимые книги</a>
                    </li>
                    <li>
                        <a href='hobby#music'>Моя любимая музыка</a>
                    </li>
                    <li>
                        <a href='hobby#films'>Мои любимые фильмы</a>
                    </li>
                </ul>
            </li>
            <li>
                <a href='photo'>Фотоальбом</a>
            </li>
            <li>
                <a href='study'>Учеба</a>
            </li>
            <li>
                <a href='contacts'>Контакты</a>
            </li>
            <li>
                <a href='test'>Тест</a>
            </li>
            <li>
                <a href='history'>
                    <span aria-hidden='true' class='fa fa-history'></span>
                </a>
            </li>
        </ul>
    </div>
    <div class='time' id='time'></div>
</header>
