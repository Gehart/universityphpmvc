<!DOCTYPE html>
<html lang="ru">

<head>
    <meta content="text/html; charset=UTF-8" http-equiv="Content-Type">
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,400;0,600;0,700;1,400&amp;display=swap" rel="stylesheet">
    <link href="public/assets/css/style.css" rel="stylesheet">
    <title><?php echo $page_title ?></title>
</head>

<body>
    <?php include 'app/views/' . $content_view; ?>
</body>

</html>