<?php
class Controller_Hobby extends Controller
{
	function __construct()
	{
		$this->model = new Model_Hobby();
		$this->view = new View();
	}

	function action_index()
	{
		$data = $this->model->get_data();
		$this->view->generate('hobby_view.php', 'template_view.php', 'Интересы', $data);
	}
}