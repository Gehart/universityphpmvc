<?php

class Controller_Contacts extends Controller
{
	function __construct()
	{
		$this->model = new Model_Contacts();
		$this->view = new View();
	}

	function action_index()
	{
		$data = [];
		
		if (!empty($_POST)) {
			if (!$this->model->validator->validate($_POST)) {
				$data = [
					'validation' => [
						'result' => false,
						'errors' => $this->model->validator->Errors
					]
				];
			}
			else {
				$data = [
					'validation' => [
						'result' => true,
						'errors' => []
					]
				];
			}
			
			$data['value'] = $_POST;
		}

		$this->view->generate('contacts_view.php', 'template_view.php', 'Контакты', $data);
	}
}
