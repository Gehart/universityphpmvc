<?php
class Controller_Test extends Controller
{
	function __construct()
	{
		$this->model = new Model_Test();
		$this->view = new View();
	}

	function action_index()
	{
		$data = [];
		
		if (!empty($_POST)) {
			if (!$this->model->validator->validate($_POST)) {
				// $this->model->validator->showErrors();
				$this->consoleTable($this->model->validator->Errors);
				$data = [
					'validation' => [
						'result' => false,
						'errors' => $this->model->validator->Errors
					]
				];
			}
			else {
				$data = [
					'validation' => [
						'result' => true,
						'errors' => []
					]
				];
			}
			
			$this->consoleTable($_POST);
			$data['value'] = $_POST;
		}

		$this->view->generate('test_view.php', 'template_view.php', 'Тест по дисциплине', $data);
	}

	function consoleLog($data) {
		echo '<script>';
		echo 'console.log(' . json_encode($data) . ')';
		echo '</script>';
	}

	function consoleTable($data) {
		echo '<script>';
		echo 'console.table(' . json_encode($data) . ')';
		echo '</script>';
	}
}