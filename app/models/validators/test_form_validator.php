<?php
require 'app/models/validators/form_validator.php';
class TestFormValidation extends FormValidation {
    public $Rules = [
        'fio' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
                ['func' => 'validateFio', 'message' => 'Введите ФИО полностью!']
            ]
        ], 
        'group' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ], 
        'eco-select' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ],
        'eco-textarea' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ], 
        'garbage' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ], 
        'email' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
                ['func' => 'checkEmail', 'message' => 'Email не корректен!'],
            ]
        ],
    ];

    // реализовать остальные функции

    function validateFio($fio) {
        $fio = trim($fio);
        return count(preg_split('/\s+/', $fio)) >= 3;
    }
}