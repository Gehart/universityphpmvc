<?php
class FormValidation {
    public $Rules = [
        'fio' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
                [ 'func' => 'validateFio', 'message' => 'Введите ФИО полностью!' ]
            ]
        ],
        'age' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
            ]
        ],
        'tel' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
                [ 'func' => 'validateTel', 'message' => 'Заполните поле!' ],
            ]
        ],
        'email' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
                [ 'func' => 'checkEmail', 'message' => 'Заполните поле!' ],
            ]
        ],
        'textarea' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
            ]
        ],
        'eco-select' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
            ]
        ],
        'eco-textarea' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
            ]
        ],
        'group' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
            ]
        ],
        'birthday' => [
            'validators' => [
                [ 'func' => 'isNotEmpty', 'message' => 'Заполните поле!' ],
            ]
        ]
    ];

    // TODO: сделать добавление ошибок в модели каждой из форм. 
    public $Errors;
    function isNotEmpty($data) {
        // echo !empty($data);
        return $data || strlen($data);
    }

    function isInteger($data) {
        return is_numeric ($data);
    }

    function isLess($data, $value) {
        return $data < $value;
    }

    function isGreater($data, $value) {
        return $data > $value;
    }

    function checkEmail($data) {
        return !!filter_var($data, FILTER_VALIDATE_EMAIL); 
    }

    function setRule($rules) {
        foreach ($rules as $key => $value) {
            $this->Rules[$key] = $value;
        }
    }

    // Валидация формы. Ошибки записываются в поле Errors
    function validate($post_array) {
        // переменная для результата валидации формы
        $hasErrors = false;

        // проходим по всем правилам заполнения полей в массив Rules
        foreach ($this->Rules as $fieldName => $field) {
            foreach ($field['validators'] as $validator) {
                $func = $validator['func'];
                // echo '<br> function ';
                // print_r($post_array);

                // Проверяем: 
                // - если ли уже ошибка в этом поле
                // - есть ли в массиве _POST это поле
                // - результат функции проверки
                if( !isset($this->Errors[$fieldName]) &&
                    (!isset($_POST[$fieldName]) || !$this->$func($post_array[$fieldName])) ) 
                {
                    $this->Errors[$fieldName] = $validator['message'];
                    $hasErrors = true;
                }
            }
        }
        return !$hasErrors;
    }

    function showErrors() {
        if(!empty($this->Errors)) {
            foreach ($this->Errors as $key => $value) {
                echo '<pre>[' . $key . '] => ' . $value . '</pre>';
            }
        }
        else {
            echo '<br>Ошибок нет';
        }
    }
}