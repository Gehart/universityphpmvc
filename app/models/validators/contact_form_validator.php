<?php
require 'app/models/validators/form_validator.php';
class ContactFormValidation extends FormValidation {
    public $Rules = [
        'fio' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
                ['func' => 'validateFio', 'message' => 'Введите ФИО полностью!']
            ]
        ], 
        'gender' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ], 
        'birthday' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ], 
        'tel' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
                ['func' => 'validateTel', 'message' => 'Телефон не корректен!'],
            ]
        ],
        'email' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
                ['func' => 'checkEmail', 'message' => 'Email не корректен!'],
            ]
        ],
        'textarea' => [
            'validators' => [
                ['func' => 'isNotEmpty', 'message' => 'Заполните поле!'],
            ]
        ],
    ];

    // реализовать остальные функции

    function validateFio($fio) {
        $fio = trim($fio);
        return count(preg_split('/\s+/', $fio)) >= 3;
    }

    function validateTel($str)
    {
        $str = trim($str);
        return preg_match('/^\+[73]\d{8,10}$/', $str);
    }
}