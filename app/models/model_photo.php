<?php
class Model_Photo extends Model
{
    public $path = 'public/assets/img/';
    public function get_data()
    {
        return array(
            array(
                'name' => $this->path.'cat1.jpg',
                'description' => 'Фото котика 1'
            ),
            array(
                'name' => $this->path.'cat2.jpg',
                'description' => 'Фото котика 2'
            ),
            array(
                'name' => $this->path.'cat3.jpg',
                'description' => 'Фото котика 3'
            ),
            array(
                'name' => $this->path.'cat4.jpg',
                'description' => 'Фото котика 4'
            ),
            array(
                'name' => $this->path.'cat5.jpg',
                'description' => 'Фото котика 5'
            ),
            array(
                'name' => $this->path.'cat6.jpg',
                'description' => 'Фото котика 6'
            ),
            array(
                'name' => $this->path.'cat7.jpg',
                'description' => 'Фото котика 7'
            ),
            array(
                'name' => $this->path.'cat8.jpg',
                'description' => 'Фото котика 8'
            ),
            array(
                'name' => $this->path.'cat9.jpg',
                'description' => 'Фото котика 9'
            ),
            array(
                'name' => $this->path.'cat10.jpg',
                'description' => 'Фото котика 10'
            ),
            array(
                'name' => $this->path.'cat11.jpg',
                'description' => 'Фото котика 11'
            ),
            array(
                'name' => $this->path.'cat12.jpg',
                'description' => 'Фото котика 12'
            ),
            array(
                'name' => $this->path.'cat13.jpg',
                'description' => 'Фото котика 13'
            ),
            array(
                'name' => $this->path.'cat14.jpg',
                'description' => 'Фото котика 14'
            ),
            array(
                'name' => $this->path.'cat15.jpg',
                'description' => 'Фото котика 15'
            ),
        );
    }
}
