$().ready(mainPhoto);
function mainPhoto() {
    // addImages();
    countImages();
    makeImageBiggerOnClickHandler();
    turnOffBigImageHandler();
    changeImageByArrows();
}

function createImgElement(filename, title, imgId) {
    return $(`  <figure class="img-block">
                    <div class="img-wrapper">
                        <img src="${filename}" id="${imgId}" alt="${title}" title="${title}">
                    </div>
                    <figcaption>${title}</figcaption>
                </figure>`);
}

let imageCount;
function countImages() {
    imageCount = $('.img-block').length;
}
// function addImages() {
//     let imgDirectory = "public/assets/img/";
//     let fileNameAndTitlesMap = new Map();
//     let countImg = 15;

//     for (let i = 0; i < countImg; i++) {
//         fileNameAndTitlesMap.set(`${imgDirectory}cat${i + 1}.jpg`, `Фото котика ${i + 1}`);
//     }

//     imageCount = fileNameAndTitlesMap.size;
//     const photoWrapper = $(".photo-wrapper");
//     let counterImg = 1;

//     fileNameAndTitlesMap.forEach((fileName, title, map) => {
//         photoWrapper.append(createImgElement(title, fileName, 'img' + (counterImg++)));
//     });
// }



function makeImageBiggerOnClickHandler() {
    const images = $(".img-wrapper img");
    for (let i = 0; i < images.length; i++) {
        images[i].addEventListener("click", showBigImage.bind(null, images[i], i));
    }
}
let currentBigImage;
function showBigImage(newImg, count) {
    setBigImg(newImg);
    currentBigImage = newImg;
    setNumberToCaption(count + 1);
    removeHiddenClassFromBigImg(true);
}
function setBigImg(newImg) {
    let bigImg = $('#big-img img');
    currentBigImage = newImg;
    bigImg.attr('src', newImg.getAttribute('src'));
    bigImg.attr('alt', newImg.getAttribute('alt'));
    bigImg.attr('title', newImg.getAttribute('title'));
}
function changeSrcForBigImage(src) {
    $("#big-img img").attr('src', src);
}
function removeHiddenClassFromBigImg(wantBeShown) {
    let bigImgEl = document.getElementById("big-img");
    let controlButtons = Array.from(document.querySelectorAll(".arrow"));
    if (wantBeShown) {
        bigImgEl.classList.remove("hidden");
    }
    // если в event.target нет элементов управления.
    else if (!controlButtons.map((el) => el.contains(event.target)).includes(true)) {
        bigImgEl.classList.add("hidden");
    }
}
function turnOffBigImageHandler() {
    let bigImgEl = document.getElementById("big-img");
    bigImgEl.addEventListener('click', (event) => {
        removeHiddenClassFromBigImg(false);
    });
}
function changeImageByArrows() {
    $('.big-img-control .arrow-left').on('click', goToImage.bind(null, -1));
    $('.big-img-control .arrow-right').on('click', goToImage.bind(null, 1));
}
function setNumberToCaption(imgNumber) {
    $('.current-img-number').html(imgNumber + '');
    $('.biggest-img-number').html(imageCount + '');
}
function goToImage(direction) {
    let imgIdNumber = +(currentBigImage.id + '').slice(3);
    let newImgIdNumber = (imgIdNumber - 0 + direction);
    let newImgId = 'img' + newImgIdNumber;
    let newImg = document.getElementById(newImgId);
    if (newImg !== null) {
        let bigImg = $('#big-img img');
        bigImg.fadeOut(200, function () {
            (newImg !== null) ? setBigImg(newImg) : null;
            bigImg.fadeIn(100);
        });
        setNumberToCaption(newImgIdNumber);
    }
}
