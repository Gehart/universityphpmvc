main();
function main() {
    counter();
    showRealTime();
    setActiveHeaderLink();
    // if (window.location.href.includes("hobby")) {
    //     generateList();
    // }
    if (window.location.pathname.includes("history")) {
        addTablesWithHistoryOfViews();
    }
    if (window.location.pathname.includes("study")) {
        study_defence();
    }
}

function setActiveHeaderLink() {
    let headerLinks = document.querySelectorAll("header ul.menu-list li a");
    headerLinks.forEach(function (linkElement) {
        let location = window.location.href;
        let link = linkElement.href;
        if (location.includes(link)) {
            linkElement.classList.add('active');
        }
    });
}

function generateList() {
    let articleTitlesAndId = getArticleTitlesAndID();
    createListWithLinkToArticles(articleTitlesAndId);
}

function getArticleTitlesAndID() {
    let articles = document.querySelectorAll(".article");
    let articleTitlesAndId = new Map();
    for (let article of articles) {
        articleTitlesAndId.set(article.id, article.querySelector("h3").innerHTML);
    }
    return articleTitlesAndId;
}
function createListWithLinkToArticles(articleTitlesAndId) {
    let ulContainer = document.querySelector(".content");
    ulContainer.append(document.createElement("ul"));
    let ul = ulContainer.querySelector("ul");
    articleTitlesAndId.forEach((value, key) => {
        let li = document.createElement("li");
        let a = document.createElement("a");
        a.setAttribute("href", "#" + key);
        a.innerHTML = value;
        ul.append(li);
        li.append(a);
    });
}

function showRealTime() {
    const timeElement = document.getElementById("time");
    refreshTime(timeElement);
}
function refreshTime(timeElement) {
    let date = new Date();
    timeElement.innerHTML = ('0' + date.getHours()).slice(-2) + ':' +
        ('0' + date.getMinutes()).slice(-2) + ' ' +
        ('0' + date.getDate()).slice(-2) + '.' +
        ('0' + (date.getMonth() + 1)).slice(-2) + '.' + date.getFullYear() +
        ', ' + convertDayOfTheWeekToString(date);
    setTimeout(refreshTime.bind(this, timeElement), 1000);
}
function convertDayOfTheWeekToString(date) {
    const dayOfWeelOnRussian = ['Воскресенье', 'Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота'];
    return dayOfWeelOnRussian[date.getDay()];
}
// document.cookie = 'fffff=notvalue';
// document.cookie = 'fff=notnotvalue';
// document.cookie = 'ffff=value';
// console.log(getCookie('fff'));
function getCookie(name) {
    const cookieValue = document.cookie
        .split('; ')
        .find(row => row.startsWith(name + '='));
    if (cookieValue != undefined) {
        return cookieValue.split('=')[1];
    }
    else {
        return null;
    }
}
function setCookie(name, value, exlpirationInDays = 365) {
    let date = new Date();
    date.setDate(date.getDate() + exlpirationInDays);
    const exlpiration = date.toUTCString();
    const constraint = `;expires=${exlpiration}; path=/;`;
    const keyAndValue = `${name}=${value}`;
    document.cookie = keyAndValue + constraint;
}
function counter() {
    const pageUrl = window.location.pathname;
    // eraseCookie(pageUrl);
    setCookie(pageUrl, Number(getCookie(pageUrl)) + 1);
    // localStorage.clear();
    localStorage.setItem(pageUrl, Number(localStorage.getItem(pageUrl)) + 1 + '');
}
function eraseCookie(name) {
    document.cookie = name + '=; Max-Age=-9999999;path=/';
}
function addTablesWithHistoryOfViews() {
    const container = ".history .container .history-table-wrapper";
    createTableTitle("Общее количество посещений", container);
    let tableCommon = createTableTag("cookie", container);
    addRecordsToTableFromCookie(tableCommon);
    createTableTitle("История текущего сеанса", container);
    let tableLocalStorage = createTableTag("local-storage", container);
    addRecordsToTableFromLocalStorage(tableLocalStorage);
}
function createTableTag(name, containerQuery) {
    const table = document.createElement("table");
    table.setAttribute("id", "table-" + name);
    table.setAttribute("class", "table table-" + name);
    table.setAttribute("border", "1");
    table.setAttribute("cellpadding", "0");
    table.setAttribute("cellspacing", "0");
    appendToContainer(table, containerQuery);
    return table;
}
function createTableTitle(title, containerQuery) {
    const titleElement = document.createElement("h3");
    titleElement.setAttribute("class", "table-title");
    titleElement.innerHTML = title;
    appendToContainer(titleElement, containerQuery);
    return titleElement;
}
function appendToContainer(element, containerQuery) {
    const container = document.querySelector(containerQuery);
    container.append(element);
}
function addRecordsToTableFromCookie(table) {
    // куки, которые содержат историю посещения
    let cookiesWithHistory = document.cookie.split("; ")
        .filter(row => /\/.+/.test(row))
        .map(el => el.split('=')[0]);
    addRowToHistoryTable(table, "Страница", "Число посещений");
    cookiesWithHistory.forEach(el => {
        addRowToHistoryTable(table, el, getCookie(el));
    });
}
function addRowToHistoryTable(table, pageTitle, count) {
    const tr = document.createElement("tr");
    const td1 = document.createElement("td");
    const td2 = document.createElement("td");
    td1.innerHTML = pageTitle;
    td2.innerHTML = count;
    tr.append(td1);
    tr.append(td2);
    table.append(tr);
}
function addRecordsToTableFromLocalStorage(table) {
    let dataWithHistory = Object.keys(localStorage).filter(row => /\/.+/.test(row));
    // console.log(dataWithHistory);
    addRowToHistoryTable(table, "Страница", "Число посещений");
    dataWithHistory.forEach(el => {
        addRowToHistoryTable(table, el, localStorage.getItem(el));
    });
}
function study_defence() {
    fillWichValues();
    const table = $('.table tbody');
    const row = $('<tr>\
                    <td></td>\
                    <td>Итого</td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                    <td></td>\
                </tr>');
    table.append(row);
    $('.table tr').append($('<td></td>'));
    $('.table tr:first-child td:last-child').html('Итого');
    countSumm();
}
function randomInteger(min, max) {
    return Math.floor(Math.random() * (max - min + 1)) + min;
}
function fillWichValues() {
    $('.table tbody tr:nth-child(n+4) td:nth-child(n+3)').html(Math.floor(Math.random() * 6) + 1 + '');
}
function countSumm() {
    const lines = $('.table tr').slice(3, 14).map(function () {
        return $(this).find('td:nth-child(n+3)');
    });
    lines.each(function (val) {
        let summ = $.makeArray($(this)).reduce((summ, item) => {
            return summ + (+(item.innerHTML));
        }, 0);
        $(this).last().html(summ.toString());
    });
    let summ = 0;
    const lastLine = $('.table tr').last().find('td');
    const nOfLines = 11;
    const nOfColumns = 12;
    for (let i = 0; i < nOfColumns; i++) {
        for (let j = 0; j < nOfLines; j++) {
            summ += Number.parseInt(lines[j][i].innerHTML);
        }
        lastLine.get(2 + i).innerHTML = summ.toString();
        summ = 0;
    }
}
