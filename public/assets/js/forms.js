'use strict';
$().ready(mainValidation);
function mainValidation() {
    let modalWindow = new ModalWindow();
    // на формы вешаем обработчики событий
    // TODO: раскоментировать строку
    setSubmitAndResetEventHandler();
    inputFieldEventHandler();
    if (window.location.href.includes("contacts")) {
        showCalendarOnClick();
    }
    // showPopup();
}

function redirectTo(href) {
    window.location.href = href;
}

function setSubmitAndResetEventHandler() {
    const form = document.forms[0];
    // если форма не прошла валидацию, отменить дальнейшие действия с формой
    // TODO: раскомментировать строку
    // form.addEventListener('submit', async function (event) {
    //     try {
    //         event.preventDefault();
    //         let modalWindow = new ModalWindow();
    //         let email = 'g.orlovi@ya.ru';
    //         if (!validateForm()) {
    //             return;
    //         }
    //         else if (await modalWindow.confirm()) {
    //             redirectTo('mailto:' + email);
    //         }
    //     }
    //     catch (e) {
    //         event.preventDefault();
    //         console.log("exc:", e);
    //     }
    // });
    // при очистке данных, также удаляем сообщения об ошибке
    form.addEventListener('reset', removeAllErrorsFromForm);
}

// Убрать сообщения об ошибке
function removeAllErrorsFromForm() {
    let fieldsWithError = document.querySelectorAll(".form .error");
    let formInputs = document.querySelectorAll(".form input");
    let formTextareas = document.querySelectorAll(".form textarea");
    // очистка сохраненных значений
    for (let i = 0; i < formInputs.length; i++) {
        formInputs[i].value = "";
        formInputs[i].setAttribute('value', '');
    }

    for (let i = 0; i < formTextareas.length; i++) {
        formTextareas[i].innerHTML = "";
    }
    
    for (let i = 0; i < fieldsWithError.length; i++) {
        fieldsWithError[i].classList.remove("error");
    }
}

function validateFio(fio) {
    return fio.trim().split(" ").length >= 3;
}

// проверяет правильность телефонного номера с помощью RegExp
function validateTel(tel) {
    const regexp = new RegExp(/^\+[73]\d{8,10}$/);
    return regexp.test(tel);
}

// валидация формы
// при незаполненном input, добавляем к нему класс error
// переводим фокус на первый незаполненный элемент    
function validateForm() {
    const formElements = document.forms[0].elements;
    let firstFieldWithError;
    removeAllErrorsFromForm();
    for (let i = 0; i < formElements.length; i++) {
        let formElement = formElements[i];
        let message = '';
        if (message = checkFormsField(formElement)) {
            setMessageOnField(formElement, message);
            if (firstFieldWithError === undefined) {
                firstFieldWithError = formElement;
            }
        }
    }
    // устанавливаем фокус на первый незаполненный элемент
    if (firstFieldWithError !== undefined) {
        firstFieldWithError.focus();
    }
    return firstFieldWithError ? false : true;
}

function isFilled(value) { return !!value; }

const formFields = [
    { fieldName: 'fio',
        validators: [{ func: isFilled, errorMessage: "Поле не заполнено!", param: '' },
            { func: validateFio, errorMessage: "Введите ФИО полностью!", param: '' },]
    },
    { fieldName: 'age',
        validators: [{ func: isFilled, errorMessage: "Выберите возраст!", param: '' }]
    },
    { fieldName: 'tel',
        validators: [{ func: isFilled, errorMessage: "Введите телефон!", param: '' },
            { func: validateTel, errorMessage: "Телефон не корректен!", param: '' }]
    },
    { fieldName: 'email',
        validators: [{ func: isFilled, errorMessage: "Заполните поле email!", param: '' }]
    },
    { fieldName: 'textarea',
        validators: [{ func: isFilled, errorMessage: "Введите сообщение!", param: '' }]
    },
    { fieldName: 'eco-select',
        validators: [{ func: isFilled, errorMessage: "Выберите вариант!", param: '' }]
    },
    { fieldName: 'eco-textarea',
        validators: [{ func: isFilled, errorMessage: "Обязательное поле!", param: '' },
            { func: validateTextarea, errorMessage: "Введите текст с записью цифры!", param: '' },]
    },
    { fieldName: 'group',
        validators: [{ func: isFilled, errorMessage: "Напишите свою группу!", param: '' }]
    },
    { fieldName: 'birthday',
        validators: [{ func: isFilled, errorMessage: "Введите дату рождения", param: '' }]
    },
];

function checkFormsField(field) {
    let fieldValidator = formFields.find(el => el.fieldName === field.name);
    let errorMessage = '';
    if (fieldValidator) {
        fieldValidator.validators.forEach(el => el.param = field.value);
        errorMessage = checkValidator(fieldValidator);
    }
    else {
        errorMessage = '';
    }
    return errorMessage;
}

function checkValidator(validatorsObject) {
    for (let i = 0; i < validatorsObject.validators.length; i++) {
        let validator = validatorsObject.validators[i];
        if (!validator.func(validator.param)) {
            return validator.errorMessage;
        }
    }
}

// проходим по всем членам и проверяем на пустоту. Сообщение берем из мапы.
// если нужны особые проверки, их сделать отдельно. 
function setMessageOnField(element, message) {
    let inputField = getParentWithInputFieldClass(element);
    if (!inputField) {
        return;
    }
    if (message != "") {
        inputField.classList.add("error");
        let errorMessage = inputField.querySelector(".error-message");
        errorMessage.textContent = message;
    }
    else {
        inputField.classList.remove("error");
    }
}

// получаем предка элемента поля ввода с классом input-field
function getParentWithInputFieldClass(el) {
    while ((el = el.parentElement) && !el.classList.contains("input-field"))
        ;
    return el;
}

function inputFieldEventHandler() {
    const form = document.forms[0].elements;
    for (let i = 0; i < form.length; i++) {
        // валидация полей при событии blur
        if (form[i].localName != 'button') {
            form[i].addEventListener('blur', function () {
                let message = checkFormsField(this);
                setMessageOnField(this, message);
            });
        }
        // TODO: тут какая-то ерунда, исправить
        // добавляем значение по умолчанию в поле телефона
        if (form[i].name === "tel") {
            form[i].addEventListener('click', function (event) {
                if (!this.value) {
                    this.setAttribute('value', '+7');
                    this.setSelectionRange(this.value.length, this.value.length);
                }
            });
        }
    }
}

function validateTextarea(text) {
    const numbersAsString = "один два три четыре пять шесть семь восемь девять десять ноль";
    let result = false;
    text.trim().split(/\s+/).forEach(element => {
        if (numbersAsString.includes(element)) {
            result = true;
        }
    });
    return result;
}

function showCalendarOnClick() {
    const ageInput = document.querySelector('.age-input > input');
    ageInput.addEventListener('click', initializeCalendar);
    function initializeCalendar() {
        let calendar = new Calendar();
        // удаляем событие для исключения повторного срабатывания
        ageInput.removeEventListener('click', initializeCalendar);
    }
}

function showPopup() {
    let popup = new PopupMessage();
}

class Calendar {
    constructor() {
        this.date = new Date();
        this.element = document.getElementById('calendar');
        this.container = this.element.parentElement;
        this.inputField = this.container.querySelector("input");
        this.monthsNames = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль',
            'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        this.pickedDay = 1;
        this.pickedMonth = 1;
        this.pickedYear = 2000;
        this.eventHandler();
        this.init();
        this.onChangeEventHandler();
    }
    init() {
        this.addMonthOptionTagToSelect();
        this.addYearOptionTagToSelect();
        this.addDayElements();
        this.refreshInputValue();
        this.element.classList.remove('hidden');
    }
    addDayElements() {
        let daysInCurrentMonth = this.countDaysInMonth();
        const dayContainer = this.element.querySelector('.pick-date');
        this.setFirstDayToRightPlace(dayContainer);
        // добавляем элементы дней к календарю
        for (let i = 0; i < daysInCurrentMonth; i++) {
            let dayEl = this._createDayElement(i + 1);
            dayContainer.append(dayEl);
            if (i + 1 === this.pickedDay) {
                this.setActive(dayEl);
            }
        }
    }
    removeDayElements() {
        const dayElements = this.element.querySelectorAll('.pick-date .day-of-month');
        dayElements.forEach(el => el.remove());
    }
    setFirstDayToRightPlace(dayContainer) {
        let dayOfWeekOfFirstDay = new Date(this.pickedYear, this.pickedMonth - 1, 1).getDay();
        // если воскресенье, т.е. 0, заменить на 7
        dayOfWeekOfFirstDay = (dayOfWeekOfFirstDay !== 0) ? dayOfWeekOfFirstDay - 1 : 6;
        // элемент отступа
        const emptySquare = dayContainer.querySelector('.empty-square');
        // отступ делаем с помощью grid
        if (dayOfWeekOfFirstDay != 0) {
            emptySquare.style.gridColumnEnd = dayOfWeekOfFirstDay + 1 + '';
            emptySquare.classList.remove('hidden');
        }
        else {
            emptySquare.classList.add('hidden');
        }
    }
    _createDayElement(numberOfDay) {
        let div = document.createElement("div");
        div.classList.add('day-of-month');
        div.innerHTML = numberOfDay + '';
        div.addEventListener('click', this.setActive.bind(this, div));
        return div;
    }
    countDaysInMonth() {
        return 33 - new Date(this.pickedYear, this.pickedMonth - 1, 33).getDate();
    }
    addMonthOptionTagToSelect() {
        let monthSelectTag = this.element.querySelector("#pick-month");
        for (let i = 0; i < this.monthsNames.length; i++) {
            monthSelectTag.append(this._createOptionTag(i + 1 + '', this.monthsNames[i]));
        }
    }
    addYearOptionTagToSelect() {
        let yearSelectTag = this.element.querySelector("#pick-year");
        const SMALLEST_YEAR = 1950;
        for (let i = SMALLEST_YEAR; i <= this.date.getFullYear(); i++) {
            yearSelectTag.append(this._createOptionTag(i + '', i + '', (i === 2000 || false)));
        }
    }
    _createOptionTag(value, innerHTML, isSelected = false) {
        let result = document.createElement("option");
        result.setAttribute("value", value);
        if (isSelected) {
            result.setAttribute("selected", isSelected.toString());
        }
        result.innerHTML = innerHTML;
        return result;
    }
    onChangeEventHandler() {
        let monthSelectTag = this.element.querySelector("#pick-month");
        let yearSelectTag = this.element.querySelector("#pick-year");
        monthSelectTag.addEventListener('change', this.ifSelectTagValueChanged.bind(this, monthSelectTag));
        yearSelectTag.addEventListener('change', this.ifSelectTagValueChanged.bind(this, yearSelectTag));
    }
    refreshInputValue() {
        let value = ('0' + this.pickedDay).slice(-2);
        value += ' ' + this.monthsNames[this.pickedMonth - 1] + ' ';
        value += ('' + this.pickedYear).slice(-2);
        this.inputField.value = value;
    }
    ifSelectTagValueChanged(selectTag) {
        if (selectTag.id === 'pick-month') {
            this.pickedMonth = Number(selectTag.value);
        }
        else if (selectTag.id === 'pick-year') {
            this.pickedYear = Number(selectTag.value);
        }
        this.removeDayElements();
        this.addDayElements();
        this.refreshInputValue();
    }
    eventHandler() {
        this.inputField.addEventListener('focus', (event) => {
            this.element.classList.remove('hidden');
        });
        document.addEventListener('mouseup', (event) => {
            this.hideCalendar(event);
        });
    }
    hideCalendar(event) {
        if (!this.element.contains(event.target) &&
            !this.inputField.contains(event.target)) {
            this.element.classList.add('hidden');
        }
    }
    setActive(dayEl) {
        this.removeActiveClassFromDayElements();
        this.pickedDay = Number(dayEl.innerHTML);
        dayEl.classList.add('active');
        this.refreshInputValue();
    }
    removeActiveClassFromDayElements() {
        let dateElements = Array.from(this.element.querySelectorAll(".pick-date .day-of-month"));
        dateElements.filter(el => el.classList.contains('active'))
            .forEach(el => el.classList.remove('active'));
    }
}

class PopupMessage {
    constructor() {
        this.popup = $('#popup');
        this.popup.hide();
        this.message = '';
        this.initEventHandler();
    }
    render() {
        // this.popup.remove();
        // this.popup.show();
        this.popup.html('<span>' + this.message + '</span>');
    }
    initEventHandler() {
        $('.input-field input, .textarea').mouseenter((e) => {
            this.message = e.target.getAttribute('placeholder');
            if (!this.message)
                return;
            this.render();
            this.popup.show({
                start: () => {
                    this.popup.hide();
                    this.popup.show({
                        queue: false
                    });
                },
            });
        });
        $('.input-field input, .textarea').mouseleave(() => {
            this.popup.hide({
                duration: 500
            });
        });
    }
}

class ModalWindow {
    constructor() {
        this.modalWindow = $('#modal-window');
        this.message = 'Отправлять форму?';
        this.render();
        // this.buttonHandler();
        this.modalWindow.hide();
    }
    
    render() {
        this.modalWindow.html(' <div class="modal-wrapper"> \
                                    <span> ' + this.message + '</span > \
                                    <div class="modal-buttons"> \
                                        <button value="yes">Да</button> \
                                        <button value="no">Нет</button> \
                                    </div> \
                                </div>');
    }

    confirm() {
        this.show();
        return new Promise((resolve) => {
            this.modalWindow.on('click', (e) => {
                this.hide();
                resolve(false);
            });
            $('.modal-buttons button').on('click', (e) => {
                if (e.target.value === 'yes') {
                    // this._result = true;
                    this.hide();
                    resolve(true);
                }
                else {
                    this.hide();
                    resolve(false);
                }
            });
        });
    }

    show() {
        this.modalWindow.show();
        $('#formContact').css('filter', 'blur(1px)');
    }

    hide() {
        this.modalWindow.hide(500);
        $('#formContact').css('filter', 'none');
    }
}
